<p align="center">
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/html5/html5-original.svg" alt="HTML5" width="25" height="25"/>
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" alt="CSS3" width="25" height="25"/>
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" alt="JavaScript" width="25" height="25"/>
</p>

# HTML, CSS & JAVASCRIPT

:flag_gb:
This repository contains code samples for building websites with HTML, CSS, and JavaScript.

:flag_pt:
Este repositório contém exemplos de código para construir sites com HTML, CSS e JavaScript.

## Table of Contents

- [Overview](#overview)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Overview

:flag_gb:
The goal of this repository is to provide developers with a starting point for building responsive and modern websites. The code samples in this repository showcase fundamental concepts in web development, such as layout design, styling, and interactivity. The code samples are designed to be simple and easy to understand, making them ideal for developers who are new to web development or looking to refresh their skills.

:flag_pt:
O objetivo deste repositório é fornecer aos desenvolvedores um ponto de partida para construir sites modernos e responsivos. Os exemplos de código neste repositório mostram conceitos fundamentais no desenvolvimento web, como design de layout, estilo e interatividade. Os exemplos de código são projetados para serem simples e fáceis de entender, tornando-os ideais para desenvolvedores que são novos no desenvolvimento web ou procurando atualizar suas habilidades.

## Installation

:flag_gb:
To use the code samples in this repository, simply clone the repository to your local machine:

:flag_pt:
Para usar os exemplos de código neste repositório, simplesmente clone o repositório para sua máquina local:

```
git clone git@gitlab.com:littledragonblue/html-css-javascript.git
```

## Usage

:flag_gb:
Each directory in this repository contains code samples for different aspects of web development. The HTML directory contains examples of basic HTML structure and markup, while the CSS directory contains examples of styling and layout. The JavaScript directory contains examples of interactivity and dynamic behavior.

To run the code samples, simply open the corresponding HTML file in your web browser.

:flag_pt:
Cada diretório neste repositório contém exemplos de código para diferentes aspectos do desenvolvimento web. O diretório HTML contém exemplos de estrutura e marcação básicas em HTML, enquanto o diretório CSS contém exemplos de estilo e layout. O diretório JavaScript contém exemplos de interatividade e comportamento dinâmico.

Para executar os exemplos de código, basta abrir o arquivo HTML correspondente em seu navegador da web.


## Contributing

:flag_gb:
We welcome contributions from the community! If you have an idea for a new code sample or would like to improve an existing one, please feel free to submit a pull request.

:flag_pt:
Se você tem uma ideia para um novo exemplo de código ou gostaria de melhorar um existente, sinta-se à vontade para enviar uma solicitação de pull.

## License

:flag_gb:
This repository is licensed under the MIT License. See [LICENSE](LICENSE) for more information.

:flag_pt:
Este repositório é licenciado sob a Licença MIT. Consulte o arquivo [LICENSE] (LICENSE) para obter mais informações.
